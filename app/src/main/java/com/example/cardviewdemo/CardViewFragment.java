package com.example.cardviewdemo;

import android.content.Context;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

// A reusable CardView component with layout
public class CardViewFragment extends Fragment {

    // Set the following properties to change the look and behaviour for this card
    // By default, category is PRODUCT, so square is grey, no icon
    // If category is set to HIGHRISK, for example, square will be red, icon will be
    // X and title will be "High risk"
    private Category category;
    public void setCategory(Category category) {
        this.category = category;
    }

    // Set this to the product name if category is PRODUCT. If category is not PRODUCT,
    // this value is ignored
    private String productName;
    public void setProductName(String productName) {
        this.productName = productName;
    }

    // If this value is set to a number >0, then the second line in the Card will be displayed
    // with the value "x ingredients" (where x is the value of numberOfIngredients). If this
    // value is set to <=0, then the second line in the Card will be invisible and the title
    // will be vertically center aligned
    private Integer numberOfIngredients;
    public void setNumberOfIngredients(Integer numberOfIngredients) {
        this.numberOfIngredients = numberOfIngredients;
    }

    // If this value is set to true, then the up/down arrow will be visible on the right side
    // of the Card. Whether it's up or down depends on the value of isExpanded
    private Boolean expandable;
    public void setExpandable(Boolean expandable) {
        this.expandable = expandable;
    }

    // If true, and isExpandable is true, then the arrow is facing down. If false, and
    // isExpandable is true, then the arrow is facing up. If isExpandable is false, this value
    // is ignored
    private Boolean expanded;
    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }

    // If true, the right arrow is visible on the right side of the Card.
    // Only one of isNavigatable and isExpandable should be true--both should not be set to true
    private Boolean navigatable;
    public void setNavigatable(Boolean navigatable) {
        this.navigatable = navigatable;
    }

    // Constructor
    public CardViewFragment(Context context) {
        this.context = context;
    }

    // Overridden Fragment methods
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_card_view, container, false);
        colouredBoxContainer = rootView.findViewById(R.id.coloured_box_container);
        categoryIcon = rootView.findViewById(R.id.icon_image_view);
        titleTextView = rootView.findViewById(R.id.title_text_view);
        detailsTextView = rootView.findViewById(R.id.details_text_view);

        // Set up the view in the CardView
        setupCardView();
        return rootView;
    }

    // Private properties
    private View rootView;
    private Context context;
    private RelativeLayout colouredBoxContainer;  // The colour of the square
    private ImageView categoryIcon;               // The icon in the square
    private TextView titleTextView;               // The card title
    private TextView detailsTextView;             // The second line, optional. Set to invisible if needed

    // Private methods
    private void setupCardView() {
        // Set up coloured box, icon and title
        switch (category) {
            case HIGHRISK:
                colouredBoxContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colourHighRisk));
                categoryIcon.setImageResource(R.drawable.icon_highrisk);
                titleTextView.setText("High risk");
                break;
            case MEDIUMRISK:
                colouredBoxContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colourMediumRisk));
                categoryIcon.setImageResource(R.drawable.icon_mediumrisk);
                titleTextView.setText("Medium risk");
                break;
            case ETHICALCONCERN:
                // TODO:...
                break;
            case ALLERGENTS:
                // TODO:...
                break;
            case NORISK:
                // TODO:...
                break;
            case PRODUCT:
            default:
                colouredBoxContainer.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colourProduct));
                categoryIcon.setImageResource(0);  // Product card view has no icon
                if (productName != null && !productName.isEmpty()) {
                    titleTextView.setText(productName);
                }
                else {
                    titleTextView.setText(null);
                }
                break;
        }

        // Set up detailed text view (second line)
        if (numberOfIngredients != null && numberOfIngredients > 0) {
            detailsTextView.setText(numberOfIngredients + " ingredients");
        }
        else {
            detailsTextView.setVisibility(View.GONE);
        }

        // TODO:... set up the right arrows if needed
    }
}
