package com.example.cardviewdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // The following lines create three CardView fragments and customizes them by setting its properties
        CardViewFragment highRiskCardView = new CardViewFragment(this);
        highRiskCardView.setCategory(Category.HIGHRISK);
        highRiskCardView.setNumberOfIngredients(20);

        CardViewFragment mediumRiskCardView = new CardViewFragment(this);
        mediumRiskCardView.setCategory(Category.MEDIUMRISK);

        CardViewFragment productCardView = new CardViewFragment(this);
        productCardView.setCategory(Category.PRODUCT);
        productCardView.setProductName("Crest Toothpaste");

        // Begin the fragment transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        // Place the fragments in the placeholder container views
        ft.replace(R.id.card_container_1, highRiskCardView);
        ft.replace(R.id.card_container_2, mediumRiskCardView);
        ft.replace(R.id.card_container_3, productCardView);

        // End the fragment transaction
        ft.commit();
    }

}
