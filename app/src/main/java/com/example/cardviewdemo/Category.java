package com.example.cardviewdemo;

// This is a list of the possible categories that a Card might display
public enum Category {
    HIGHRISK, MEDIUMRISK, ETHICALCONCERN, ALLERGENTS, NORISK, PRODUCT
}
